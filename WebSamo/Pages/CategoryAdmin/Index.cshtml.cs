using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Storage.Data;
using Storage.Models;

namespace WebSamo.Pages.CategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        public IndexModel(Storage.Data.WebSamoContext context)
        {
            _context = context;
        }

        public IList<Category> Category { get;set; }

        public async Task OnGetAsync()
        {
            Category = await _context.Newcat.ToListAsync();
        }
    }
}
