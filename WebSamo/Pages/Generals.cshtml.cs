﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Storage.Data;
using Storage.Models;

namespace WebSamo.Pages
{
    public class GeneralsModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        public GeneralsModel(Storage.Data.WebSamoContext context)
        {
            _context = context;
        }

        public IList<Base> Base { get;set; }
        public IList<Category> Category { get;set; }

        public async Task OnGetAsync()
        {
            Base = await _context.Newbase.ToListAsync();
        }
    }
}
