using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Storage.Data;
using Storage.Models;

using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace WebSamo.Pages.NewAdmin
{
    public class CreateModel : PageModel
    {
        public IActionResult OnGet()
        {
            ViewData["Name"] = new SelectList(_context.Newcat, "Name", "Name");
            return Page();

        }

        private readonly Storage.Data.WebSamoContext _context;

        private IHostingEnvironment _environment;

        public CreateModel(Storage.Data.WebSamoContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        [BindProperty]
        public Base Base { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            IFormFile ImageFile = Request.Form.Files[0];

            var file = Path.Combine(_environment.ContentRootPath, "wwwroot/images/NewAdmin", ImageFile.FileName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                await ImageFile.CopyToAsync(fileStream);
            }
            
            Base.imageFileName =  Request.Form.Files[0].FileName;

            _context.Newbase.Add(Base);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}