using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Storage.Data;
using Storage.Models;

using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace WebSamo.Pages.NewAdmin
{
    public class EditModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        private IHostingEnvironment _environment;

        public EditModel(Storage.Data.WebSamoContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        [BindProperty]
        public Base Base { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            ViewData["Name"] = new SelectList(_context.Newcat, "Name", "Name");
            if (id == null)
            {
                return NotFound();
            }

            Base = await _context.Newbase.FirstOrDefaultAsync(m => m.BaseID == id);

            if (Base == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Base).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BaseExists(Base.BaseID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            IFormFile ImageFile = Request.Form.Files[0];

            var file = Path.Combine(_environment.ContentRootPath, "wwwroot/images/NewAdmin", ImageFile.FileName);
            using (var fileStream = new FileStream(file, FileMode.Create))
            {
                await ImageFile.CopyToAsync(fileStream);
            }
            
            Base.imageFileName =  Request.Form.Files[0].FileName;

            _context.Newbase.Attach(Base);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }

        private bool BaseExists(int id)
        {
            return _context.Newbase.Any(e => e.BaseID == id);
        }
    }
}
