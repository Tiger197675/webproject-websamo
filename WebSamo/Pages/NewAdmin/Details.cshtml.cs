using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Storage.Data;
using Storage.Models;

namespace WebSamo.Pages.NewAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        public DetailsModel(Storage.Data.WebSamoContext context)
        {
            _context = context;
        }

        public Base Base { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Base = await _context.Newbase.FirstOrDefaultAsync(m => m.BaseID == id);

            if (Base == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
