using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Storage.Data;
using Storage.Models;

using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace WebSamo.Pages
{
    public class FormPlaceModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        public FormPlaceModel(Storage.Data.WebSamoContext context)
        {
            _context = context;
        }

   
        public String FormName;
        public String FormId;
        public String FormBase;
        public String FormStork;
    
            
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            FormName = Request.Form["FormName"];
            FormId = Request.Form["FormId"];
            FormBase = Request.Form["FormBase"];
            FormStork = Request.Form["FormStork"];

            Form Uform = new Form();
            Uform.FormName = FormName;
            Uform.FormId = FormId;
            Uform.FormBase = FormBase;
            Uform.FormStork = FormStork;

            _context.NewForm.Add(Uform);
            await _context.SaveChangesAsync();

            return Page();
        }
    }
}