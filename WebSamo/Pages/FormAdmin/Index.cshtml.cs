using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Storage.Data;
using Storage.Models;

namespace WebSamo.Pages.FormAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        public IndexModel(Storage.Data.WebSamoContext context)
        {
            _context = context;
        }

        public IList<Form> Form { get;set; }

        public async Task OnGetAsync()
        {
            Form = await _context.NewForm.ToListAsync();
        }
    }
}
