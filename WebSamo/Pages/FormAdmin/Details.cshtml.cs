using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Storage.Data;
using Storage.Models;

namespace WebSamo.Pages.FormAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Storage.Data.WebSamoContext _context;

        public DetailsModel(Storage.Data.WebSamoContext context)
        {
            _context = context;
        }

        public Form Form { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Form = await _context.NewForm.FirstOrDefaultAsync(m => m.FormId == id);

            if (Form == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
