using Storage.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Storage.Data
{
    public class WebSamoContext : IdentityDbContext<User>
    {
        public DbSet<Category> Newcat { get; set; }
        public DbSet<Base> Newbase { get; set; }
        public DbSet<Form> NewForm { get; set; }
		
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=WebSamo.db");			
        }
    }
}