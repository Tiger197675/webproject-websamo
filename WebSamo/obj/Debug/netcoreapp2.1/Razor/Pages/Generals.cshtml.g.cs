#pragma checksum "E:\WebSamo\Pages\Generals.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "defc9ecad8e27cb7a8f8aba16b190af54687f9fb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(WebSamo.Pages.Pages_Generals), @"mvc.1.0.razor-page", @"/Pages/Generals.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Generals.cshtml", typeof(WebSamo.Pages.Pages_Generals), null)]
namespace WebSamo.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\WebSamo\Pages\_ViewImports.cshtml"
using WebSamo;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"defc9ecad8e27cb7a8f8aba16b190af54687f9fb", @"/Pages/Generals.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3e551bb17e1b20363cbabc02e9b3ec70fae18eac", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Generals : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/home/1.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/home/2.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/home/3.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "E:\WebSamo\Pages\Generals.cshtml"
  
    ViewData["Title"] = "WebSamo - อื่นๆ";

#line default
#line hidden
            BeginContext(77, 506, true);
            WriteLiteral(@"<br>
<div class=""container""> 
  <div id=""carousel-example-generic"" class=""carousel slide"" data-ride=""carousel"">
      <ol class=""carousel-indicators"">
        <li data-target=""#carousel-example-generic"" data-slide-to=""0"" class=""active""></li>
        <li data-target=""#carousel-example-generic"" data-slide-to=""1""></li>
        <li data-target=""#carousel-example-generic"" data-slide-to=""2""></li>
      </ol>

    <div class=""carousel-inner"" role=""listbox"">
      <div class=""item active"">
        ");
            EndContext();
            BeginContext(583, 31, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "cdb10ffbb01b4e55acf1fb641114d2f1", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(614, 50, true);
            WriteLiteral("\r\n      </div>\r\n      <div class=\"item\">\r\n        ");
            EndContext();
            BeginContext(664, 31, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "93ab0f8d479747d19809677fb9784ddd", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(695, 50, true);
            WriteLiteral("\r\n      </div>\r\n      <div class=\"item\">\r\n        ");
            EndContext();
            BeginContext(745, 31, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "0d45c2d2493d470ba8e48fbfda7c43e6", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(776, 1338, true);
            WriteLiteral(@"
      </div>
    </div>

    <a class=""left carousel-control"" href=""#carousel-example-generic"" role=""button"" data-slide=""prev"">
      <span class=""glyphicon glyphicon-chevron-left"" aria-hidden=""true""></span>
      <span class=""sr-only"">ก่อนหน้า</span>
    </a>
    <a class=""right carousel-control"" href=""#carousel-example-generic"" role=""button"" data-slide=""next"">
      <span class=""glyphicon glyphicon-chevron-right"" aria-hidden=""true""></span>
      <span class=""sr-only"">ถัดไป</span>
    </a>
  </div>    
</div>

<br>
<br>


<div class=""container-fluid"">
  <div class=""row"">
    <div class=""col-sm-2 sidebar"">
    <br><br><Br><Br>
      <div class=""list-group"">
        <a href=""/Index"" class=""list-group-item active"">หน้าหลัก</a>
        <a href=""/Office"" class=""list-group-item"">อุปกรณ์สํานักงาน</a>
        <a href=""/Container"" class=""list-group-item"">บรรจุภัณฑ์</a>
        <a href=""/Kitchen"" class=""list-group-item"">เครื่องครัว</a>
        <a href=""/Tools"" class=""list-group-item"">เครื่");
            WriteLiteral(@"องมือช่าง</a>
        <a href=""/Sports"" class=""list-group-item"">อุปกรณ์กีฬา</a>
        <a href=""/Generals"" class=""list-group-item"">อื่นๆ</a>
      </div>
    </div>
    <div class=""col-sm-10"">
      <div class=""col-sm-13"">
        <h2>อื่นๆ</h2>
        <hr>
      </div>
      <div class=""col-sm-13"">
");
            EndContext();
#line 62 "E:\WebSamo\Pages\Generals.cshtml"
         foreach (var item in Model.Base) {
            if("อื่นๆ" == item.Name ){

#line default
#line hidden
            BeginContext(2199, 152, true);
            WriteLiteral("        <div class=\"col-sm-3\">\r\n          <div class=\"thumbnail\">\r\n            <br>\r\n            <center><font face=\"PSU Stidti Lighto\">\r\n              ");
            EndContext();
            BeginContext(2352, 43, false);
#line 68 "E:\WebSamo\Pages\Generals.cshtml"
         Write(Html.DisplayFor(modelItem => item.BaseName));

#line default
#line hidden
            EndContext();
            BeginContext(2395, 66, true);
            WriteLiteral("\r\n            </font></center>\r\n            <hr>\r\n            <img");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 2461, "\"", 2534, 2);
            WriteAttributeValue("", 2467, "./images/NewAdmin/", 2467, 18, true);
#line 71 "E:\WebSamo\Pages\Generals.cshtml"
WriteAttributeValue("", 2485, Html.DisplayFor(modelItem => item.imageFileName), 2485, 49, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2535, 116, true);
            WriteLiteral(" width=\"150\" >\r\n            <hr>\r\n            <font face=\"PSU Stidti Lighto\">\r\n              &nbsp;&nbsp;หมวดหมู่ : ");
            EndContext();
            BeginContext(2652, 39, false);
#line 74 "E:\WebSamo\Pages\Generals.cshtml"
                                Write(Html.DisplayFor(modelItem => item.Name));

#line default
#line hidden
            EndContext();
            BeginContext(2691, 40, true);
            WriteLiteral(" <br>\r\n              &nbsp;&nbsp;รหัส : ");
            EndContext();
            BeginContext(2732, 41, false);
#line 75 "E:\WebSamo\Pages\Generals.cshtml"
                            Write(Html.DisplayFor(modelItem => item.BaseID));

#line default
#line hidden
            EndContext();
            BeginContext(2773, 41, true);
            WriteLiteral(" <br>\r\n              &nbsp;&nbsp;จำนวน : ");
            EndContext();
            BeginContext(2815, 40, false);
#line 76 "E:\WebSamo\Pages\Generals.cshtml"
                             Write(Html.DisplayFor(modelItem => item.Stork));

#line default
#line hidden
            EndContext();
            BeginContext(2855, 62, true);
            WriteLiteral(" <br>\r\n            </font>\r\n          </div>\r\n        </div>\r\n");
            EndContext();
#line 80 "E:\WebSamo\Pages\Generals.cshtml"
            }
            }

#line default
#line hidden
            BeginContext(2947, 46, true);
            WriteLiteral("      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
