using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Storage.Models
{
    public class User : IdentityUser{
    public string FirstName { get; set;}
    public string LastName { get; set;}
    }

    public class Category {
        public int CategoryID { get; set;}
        public string Name { get; set;}
    }
    
    public class Base {
        public int BaseID { get; set; }
        public string BaseName { get; set; }
        public string Name { get; set;}
        public Category BaseCat { get; set; }
        public int Stork { get; set; }
        public string  imageFileName {get; set;}

        public string UserId {get; set;}
        public User BaseUser {get; set;}
    }

    public class Form {
        public string FormName {get; set;}
        public string FormId { get; set;}
        public string FormBase {get; set;}
        public Base FormCat { get; set;}
        public string FormStork {get; set;}
        public Base FormNum {get; set;}
            }

}
